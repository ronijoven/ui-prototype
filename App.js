
import React from 'react';
import {
  AsyncStorage,
  TabNavigator,
  StackNavigator
} from 'react-navigation';
import { connect } from 'react-redux'

import splash from './App/Screens/Splash/splash';
import firstcode from './App/Screens/Intro/firstcode';
import codeok from './App/Screens/Intro/codeok';
import inform from './App/Screens/Intro/inform';
import askinform from './App/Screens/Intro/askinform';
import closeapp from './App/Components/CloseApp';
import login from './App/Screens/Login/login';
import dashboard from './App/Components/SetDashboard';
import register from './App/Components/RegisterTabs';
import newteam from './App/Screens/Team/newteam';
import confirmteam from './App/Screens/Team/confirmteam';

let gUserData = {
   language: 'en',
   username: '',
   password: '',
   usertype: '',
   userteam: ''
};
let gSessionData = {
   radius: {
      '0':'Select Radius',
      '25':'25 km',
      '50':'59 km',
      '100':'100 km',
      '150':'150 km',
      '200':'200 km'
   }
};

//AsyncStorage.setItem('GlobalData', JSON.stringify(gUserData),
//  () => { AsyncStorage.mergeItem('GlobalData', JSON.stringify(gSessionData),
//  () => { AsyncStorage.getItem('GlobalData', (err, result) => { console.log(result); }); }); });

class HomePage extends  React.Component {
  static navigationOptions = {
    title: 'Home',
  }
}

const PageStack = StackNavigator({
    Splash: {
        screen: splash,
        navigationOptions: ({navigation}) => ({
            header: false
        }),
        title:'Welcome'
    },
    FirstCode: {
        screen: firstcode,
        navigationOptions: ({navigation}) => ({
            header: true
        }),
        headerMode: 'screen',
        header : null
    },
    CodeOk: {
        screen: codeok,
        navigationOptions: ({navigation}) => ({
            header: true
        }),
        headerMode: 'none',
        header : null
    },
    AskInform: {
        screen: askinform,
        navigationOptions: ({navigation}) => ({
            header: true
        }),
        headerMode: 'none',
        header : null
    },
    Inform: {
        screen: inform,
        navigationOptions: ({navigation}) => ({
            header: true
        }),
        headerMode: 'none',
        header : null
    },
    Login: {
        screen: login,
        navigationOptions: ({navigation}) => ({
            header: true
        }),
        headerMode: 'none',
        header : null
    },
    Register: {
        screen: register,
        navigationOptions: ({navigation}) => ({
            header: true
        }),
        headerMode: 'none',
        header : null
    },
    Dashboard: {
        screen: dashboard,
        navigationOptions: ({navigation}) => ({
            header: true
        }),
        headerMode: 'none',
        header : null
    },
    NewTeam: {
      screen: newteam,
      navigationOptions: ({navigation}) => ({
          header: true
      }),
      headerMode: 'none',
      header : null
    },
    ConfirmTeam: {
      screen: confirmteam,
      navigationOptions: ({navigation}) => ({
          header: true
      }),
      headerMode: 'none',
      header : null
    },

    CloseApp: {
      screen: closeapp,
      navigationOptions: ({navigation}) => ({
          header: true
      }),
      headerMode: 'none',
      header : null
    }
  },
  {
    headerMode: 'none',
    mode: 'modal'
});

export default PageStack;
