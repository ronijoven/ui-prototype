import React from 'react';
import { AppRegistry } from 'react-native';
import straatinfo from './index.js';

AppRegistry.registerComponent('straatinfo', () => straatinfo);
