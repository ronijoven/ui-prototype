/* @flow */

import React from 'react';
import {
  Image,Dimensions,
  Platform,
  StyleSheet, Text, View,TouchableHighlight } from 'react-native';

export default class Banner extends React.Component {

  render() {
    const { onPress, onBack, title, parent } = this.props;

    return (
        <View style={{ zIndex:1, flex:1, flexDirection: "row", justifyContent:'space-between',height:60, backgroundColor: '#121ab1' }}>
            <View style={{ paddingTop:5 }}>
              <TouchableHighlight src='chat' onPress={onBack}>
                <Image style={{ }} source={require('../assets/chat_icon.png')} />
              </TouchableHighlight>
            </View>
            <View style={{ zIndex:1 }}>
              <Text style={{ color:'white', fontSize: 14, paddingTop:5 }}>{title}</Text>
            </View>
            <View style={{ zIndex:1 }}>
              <TouchableHighlight onPress = {onPress}>
                <Image source={require('../assets/menu_icon.png')}
                  style={{ marginTop:5, width:30,  height: 20, marginRight:5, resizeMode:'contain' }}/>
              </TouchableHighlight>
            </View>
        </View>
    );
  }
}

//alignItems: 'center', justifyContent: 'center'
// alignItems: 'flex-end', justifyContent: 'flex-end'
