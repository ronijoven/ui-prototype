/* @flow */

import React from 'react';
import {
  Image,Dimensions,
  Platform,
  StyleSheet, Text, View,TouchableHighlight } from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class Banner extends React.Component {

  render() {
    const { onPress, onBack, title, parent } = this.props;

    return (
      <View style={{height:150,}}>
        <View style={{ flex:1, flexDirection: "row", justifyContent:'space-between', backgroundColor: '#121ab1' }}>
          <View style={{ paddingTop:5 }}>
              <TouchableHighlight src='back' onPress={onBack}>
                <MaterialIcons
                  name='reply' size={24}   background='transparent'
                  style={{ paddingLeft:10, paddingTop: 5, color: 'white' }} >
                </MaterialIcons>
              </TouchableHighlight>
          </View>
          <View style={{ paddingTop:5 }}>
            <Text style={{ color:'white', fontSize: 14, paddingTop:5 }}>{title}</Text>
          </View>
          <View style={{ paddingTop:5 }}>
            <TouchableHighlight onPress = {onPress}>
              <Image source={require('../assets/menu_icon.png')}
                style={{ marginTop:5, width:30,  height: 20, marginRight:5, resizeMode:'contain' }}/>
            </TouchableHighlight>
          </View>
        </View>

        <View style={{ flex:1,  flexDirection: 'row', justifyContent:'space-around', backgroundColor: '#121ab1'  }}>
          <View style={{ width:'30%'}}>
            <Text style={{marginLeft:10,textAlign:'center',color:'white',fontSize: 14 }}>Verdachte situatie</Text>
          </View>
          <View style={{ }}>
            <Text style={{width:250, textAlign:'center',color:'white',fontSize: 14 }}>Openbare ruimte</Text>
          </View>
          <View style={{ width:'20%'}}>
            <Text style={{ color:'white',fontSize: 14, paddingBottom:25, paddingRight:30
            }}>Chat</Text>
          </View>
        </View>

        <View style={{ flex:1, flexDirection: 'row', justifyContent: 'center', height:30 }}>
         <Image source={require('../assets/down_icon.png')}
             style={{ width:40,  height:10 }}/>
        </View>

      </View>
    );
  }
}
