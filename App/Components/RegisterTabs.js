import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';
import { TabNavigator } from 'react-navigation';

import RegistrationStepOne from "../Screens/Register/tabone";
import RegistrationStepTwo from "../Screens/Register/tabtwo";
import RegistrationStepThree from "../Screens/Register/tabthree";

export default TabNavigator({
    TabOne: {
      screen: RegistrationStepOne
    },
    TabTwo: {
      screen: RegistrationStepTwo
    },
    TabThree: {
      screen: RegistrationStepThree
    },
}, {
  tabBarPosition: 'top',
  animationEnabled: false,
  tabBarOptions: {
    tabStyle: {
      paddingBottom:10,
      paddingTop:0,
      height:45
    },
    labelStyle: {
       fontSize: 18,
    },
    style: {
      marginTop:90,
      borderRadius: 10,
    },
    activeTintColor: 'white',
    inactiveTintColor: 'black',
    activeBackgroundColor: '#b4b4b4'
  },
});


//Tabs.navigationOptions = {
//    title: "Registration"
//};

//export default Tabs;
