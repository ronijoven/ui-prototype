/* @flow */

import React from 'react';
import {
  Image,Dimensions,
  Platform,
  StyleSheet, Text, View,TouchableHighlight } from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default class Banner extends React.Component {

  render() {
    const { onPress, onBack, title, parent } = this.props;

    return (
        <View style={{ backgroundColor: '#121ab1', width: '100%', height:40 }}>
        <View style={{ flex:1, flexDirection: 'row', justifyContent:'space-between'}}>
          <TouchableHighlight src='back' onPress={onBack}>
            <MaterialIcons
              name='reply' size={24}   background='transparent'
              style={{ paddingLeft:10, paddingTop: 5, color: 'white' }} >
            </MaterialIcons>
          </TouchableHighlight>
          <View style={{ }}>
            <Text style={{ color:'white', fontSize: 14, paddingTop:5 }}>{title}</Text>
          </View>
          <TouchableHighlight onPress = {onPress}>
          <Image source={require('../assets/menu_icon.png')}
            style={{ marginTop:10, width:30,  height: 20, marginRight:5, resizeMode:'contain' }}/>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}
