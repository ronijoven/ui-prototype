import React, { Component } from 'react';
import {
  Button,Text,Platform,ScrollView,StyleSheet
}  from 'react-native';
import { DrawerNavigator } from 'react-navigation';

import Banner from './Banner';
import HomeScreen from '../Screens/Dashboard/dashboard';
import ReportScreen from '../Screens/Reports/reportdetails';
import TeamScreen from '../Components/TeamDashboard';
//import TeamScreen from '../Screens/Team/team';
import ProfileScreen from '../Screens/Profile/profile';
import SettingsScreen from '../Screens/Settings/settings';
import AboutScreen from '../Screens/About/about';
import FeedbackScreen from '../Screens/Feedback/feedback';
import ChatScreen from '../Screens/Chat/chat';
import LogoutScreen from '../Components/CloseApp';

/* Global settings */
var lang = 'nl';
global.lang = lang;

const DrawerExample = DrawerNavigator({
    Home: {
      path: '/',
      screen: HomeScreen,
    },
    Reports: {
      path: '/',
      screen: ReportScreen,
    },
    Team: {
      path: '/',
      screen: TeamScreen,
    },
    Profile: {
      path: '/',
      screen: ProfileScreen,
    },
    Settings: {
      path: '/',
      screen: SettingsScreen,
    },
    About: {
      path: '/',
      screen: AboutScreen,
    },
    Feedback: {
      path: '/',
      screen: FeedbackScreen,
    },
    Chat: {
      path: '/',
      screen: ChatScreen,
    },
    Logout: {
      path: '/',
      screen: LogoutScreen,
    }
  },
  {
    initialRouteName: 'Home',
    drawerPosition: 'right'
  }
);

export default DrawerExample;

'#999af2'
