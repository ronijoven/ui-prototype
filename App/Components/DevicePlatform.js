import { Platform } from 'react-native';

const settings = {
  platform:
    Platform.select({
        ios: "ios",
        android: 'android',
    }),
  language: {
    nl: {
       home: "Home",
       reports: "Mijn meldingen",
       team: "Mijn team",
       profile: "Profiel",
       settings: "Settings",
       aboutus: "Over ons",
       feedback: "Feedback",
       logout: "Logout"
    },
    en: {
      home: "Home",
      reports: "Reports",
      team: "Team",
      profile: "Profile",
      settings: "Settings",
      aboutus: "About Us",
      feedback: "Feedback",
      logout: "Logout"
    }
  }
}

export default settings;
