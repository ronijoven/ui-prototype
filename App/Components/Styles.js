import {
  StyleSheet,
  Platform,
  Dimensions
} from 'react-native';

const styles = StyleSheet.create({
  init: {
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#3E3E3E'
  },
  modalbg: {
    marginTop: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#3E3E3E',
    height:'100%'
  },
  container : {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#3E3E3E',
    flexDirection: 'column',
    height: 650,
    marginTop: Platform.OS === 'ios' ? 20 : 0
  },
  upperbox : {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
  },
  body: {
    marginTop: 20,
    paddingRight: 20,
    paddingLeft: 20
  },
  bodyText: {
     fontSize: 14,
     color: '#fff'
  },
  hr: {
    width:'100%',
    marginTop:20,
    borderBottomColor: '#d0d0d0',
    borderBottomWidth: 2
  },
  h1: {
    color: 'white',
    fontSize: 40,
    textAlign: 'center',
    width: '100%'
  },
  h2: {
    color: 'white',
    fontSize: 30
  },
  h3: {
    color: 'white',
    fontSize: 20
  },
  spacing: {
      height:20,
      backgroundColor: 'transparent'
  },
  minispacing: {
      height:10,
      backgroundColor: 'transparent'
  },
  sidemenuLogo: {
    width:40,
    height:40
  },
  btnRed: {
    backgroundColor:'red',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },
  btnGreen: {
    backgroundColor:'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },
  btnBlue: {
    backgroundColor:'blue',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },
  textInputContainer : {
      padding: 5,
      backgroundColor: '#FFFFFF',
      borderRadius: 5,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#bdcadc',
      shadowOffset: { width: 0, height: 1 },
      shadowRadius: 5,
      shadowOpacity: 1.0,
      justifyContent: 'center',
      flexDirection: 'row',
  },
  verticalFieldsSpacing: {
      height:20,
      backgroundColor: 'transparent'
  }
});

export default styles;
