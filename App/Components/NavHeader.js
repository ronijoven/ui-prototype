import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  StyleSheet
} from 'react-native';

class NavHeader extends Component {

    componentDidMount() {
        const { navigate } = this.props.navigation;
    }

    componentWillUnmount() {

    }

    render() {
        return (
            <View style={pagestyles.container}>
                <Image source={require('../../assets/chat_icon.png')}
                  style={pagestyles.icon} />
                <Text>{"Straat.info?"}</Text>
                <Image source={require('../../assets/menu_icon.png')}
                  style={pagestyles.icon} />
            </View>
        );
    }
}

export default FirstCode;

const pagestyles = StyleSheet.create({
  container: {
    backgroundColor:'grey'
  },
  logo: {
    height:150,
    width:150,
    margin:5,
    resizeMode: 'contain'
  },
  upperbox : {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
  }
});
