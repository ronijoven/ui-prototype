import React from 'react';
import { StackNavigator } from 'react-navigation';

import team from '../Screens/Team/team';
import newteam from '../Screens/Team/newteam';
import changeteam from '../Screens/Team/changeteam';
import teamleader from '../Screens/Team/teamleader';
import singleteam from '../Screens/Team/singleteam';
import jointeam from '../Screens/Team/jointeam';

class TeamDashboard extends  React.Component {
  static navigationOptions = {
    title: 'Home',
  }
}

const TeamPageStack = StackNavigator({
    Team: {
        screen: team,
        navigationOptions: ({navigation}) => ({
            header: false
        }),
        title:'Team'
    },
    TeamLeader: {
        screen: teamleader,
        navigationOptions: ({navigation}) => ({
            header: false
        }),
        title:'Team Leader'
    },
    NewTeam: {
        screen: newteam,
        navigationOptions: ({navigation}) => ({
            header: false
        }),
        title:'New Team'
    },
    JoinTeam: {
        screen: jointeam,
        navigationOptions: ({navigation}) => ({
            header: false
        }),
        title:'New Team'
    },
    ChangeTeam: {
        screen: changeteam,
        navigationOptions: ({navigation}) => ({
            header: false
        }),
        title:'Change Team'
    },
    SingleTeam: {
        screen: singleteam,
        navigationOptions: ({navigation}) => ({
            header: false
        }),
        title:'Change Team'
    }
  },
  {
    headerMode: 'none',
    mode: 'modal'
});

export default TeamPageStack;
