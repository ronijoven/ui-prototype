import React, { Component } from 'react';
import {
  View,
  Platform,
  Text,
  Image,
  StyleSheet
} from 'react-native';

import styles from './Styles';

class CloseApp extends Component {
    static navigationOptions = {
      tabBarLabel: 'Logout',
      drawerIcon: ({tintColor}) => {
        return (
          <Image source={require('../assets/i_logout.png')}
          style={styles.sidemenuLogo} />
        );
      }
    }

    componentDidMount() {
        const { navigate } = this.props.navigation;
        interval = setInterval(() => {
           navigate('Login')
           clearInterval(interval);
        }, 2000);
    }

    render() {
        return (
              <View style={pagestyles.container}>
                <Text style={
                  { textAlign: 'center', fontSize: 18, color:'black' }}
                >{"Thank you for using Straat.info"}</Text>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
                <View style={styles.spacing}></View>
              </View>
        );
    }
}

export default CloseApp;

const pagestyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop:'50%'
  }

});
