import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  Modal,
  StyleSheet,
  ScrollView
}  from 'react-native';
import {
  Text,  Button, CheckBox
} from 'react-native-elements';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class AboutScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Home',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_info.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props){
    super(props)
    this.state = {
      accepted: false,
      submitbtn: false,
    };
  }

  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style={{
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%'
        }}>
        <Banner title="Over ons"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Home') }
        />

        <View style ={pagestyles.container}>
          <View style={{
            width: '90%',
            paddingLeft:5,
            paddingRight:10,
            marginTop: 20

            }}>
            <Text style={{
                fontSize: 18,
              }}>
              {"Straat.info is ontwikkeld, getest en gebruikt met buurtpreventieteams in hun dagelijkse werk. We doen ons best om uw gebruikerservaring zo hoog mogelijk te laten zijn."}
            </Text>
          </View>
          <View style={{
              width: '90%',
              paddingLeft:5,
              paddingRight:10,
              marginTop: 20
            }}>
            <Text style={{
                fontSize: 18,
              }}>
              {"Als u vragen heeft of tips om de app te verbeteren dan zien we erg geïnteresseerd. Laat ons weten."}
            </Text>
          </View>
          <View style={{
              width: '90%',
              paddingLeft:5,
              paddingRight:10,
              marginTop: 20
            }}>
            <Button
              disabled={this.state.submitbtn}
              disabledStyle={{
                backgroundColor:'#9c9c9c'
              }}
              onPress={() => {
                this.props.navigation.navigate('Feedback')
              }}
              buttonStyle={pagestyles.btnSubmit}
              title="Versturen"
            />
            </View>

        </View>

      </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
