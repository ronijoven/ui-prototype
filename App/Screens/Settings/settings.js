import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  Switch,
  StyleSheet,
  Picker,
  TouchableOpacity,
  ScrollView
}  from 'react-native';
import {
  Text,
  Button
} from 'react-native-elements';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';
import Modal from 'react-native-modal'

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};
const options =["Select Radius","25 km","50 km","100 km","200 km"];

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Home',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_settings.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }
  constructor(props){
      super(props);
      this.state = {
        visibleModal: 0,
        selectedRadius: 'Select Radius'
      };
  }

  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style={{
          backgroundColor: '#f5f5f5',
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%',
          height:'100%'
        }}>
        <Banner parent='Welcome' title="Settings"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Home') }
        />
        <ScrollView>
        <Modal
            isVisible={this.state.visibleModal === 1}
            animationIn={'slideInUp'}
            animationOut={'slideOutRight'}>
            <View style={pagestyles.modalContent}>
                <View style={{height:50}}>
                  <Text>Please Select Team</Text>
                  <View style={styles.hr}></View>
                </View>
                <Picker
                  style={pagestyles.picker}
                  selectedValue={this.state.selectedRadius}
                  onValueChange={ (itemValue, itemIndex) => this.setState({selectedRadius: itemValue})}>
                  {options.map((item, index) => {
                      return (<Picker.Item label={item} value={item} key={index}/>)
                  })}
                </Picker>
                <Button
                  buttonStyle={styles.btnGreen}
                  onPress={() => this.setState({visibleModal: 0})}
                  title="Ok"
                />
             </View>
        </Modal>
        <View style ={{
              width: '100%',
              marginTop: 80,
            }}>

            <View style={{
              backgroundColor:"white",
              marginBottom: 10
              }}>

              <View style ={{
                  marginLeft: 20,
                  marginRight: 20,
                  height:50,
                  width: '90%',
                  flex:1,
                  flexDirection:'row'
                }}>
                <Text style={pagestyles.switchlabel}>Geluid</Text>

                <View style ={pagestyles.switchcoontainer}>
                  <Switch
                    tintColor="#49aff0"
                    onTintColor="#49aff0"
                    thumbTintColor="white"
                    disabled={true}
                    style={pagestyles.switchcontent}
                    value={true}
                  />
                </View>
              </View>
            </View>

            <View style={{
                backgroundColor:"white",
                marginBottom: 10
              }}>
              <View style ={{
                  marginLeft: 20,
                  height:50,
                  width: '90%',
                  flex:1,
                  flexDirection:'row'
                }}>
                <Text
                  style={pagestyles.switchlabel}
                >Trillengen</Text>
                <View style ={pagestyles.switchcoontainer}>
                  <Switch
                    tintColor="#49aff0"
                    onTintColor="#49aff0"
                    thumbTintColor="white"
                    disabled={true}
                    style={pagestyles.switchcontent}
                    value={true}
                  />
                </View>
              </View>
            </View>

            <View style={{
                backgroundColor:"white",
                marginBottom: 10
              }}>
              <View style ={{
                  marginLeft: 20,
                  height:50,
                  width: '90%',
                  flex:1,
                  flexDirection:'row'
                }}>
                <Text
                style={pagestyles.switchlabel}
                >Notificiates</Text>
                <View style ={pagestyles.switchcoontainer}>
                  <Switch
                    tintColor="#49aff0"
                    onTintColor="#49aff0"
                    thumbTintColor="white"
                    disabled={true}
                    style={pagestyles.switchcontent}
                    value={true}
                  />
                </View>
              </View>
            </View>

            <View style={{
              backgroundColor:"white", width:'100%'
              }}>
              <View style ={{
                  flex:1,
                  flexDirection:'row',
                }}>
                <View style={{
                    flex:1,
                    marginLeft: 20,
                    height:50,
                    width: '100%',
                    justifyContent:'flex-start'
                  }}>
                  <Text
                    style={pagestyles.switchlabel}
                  >{"GPS Radius"}</Text>
                </View>

                <View style ={{
                    flex:1,justifyContent:'flex-end',width:'199%'
                  }}>
                  <View style={{flex:2, flexDirection:'row',marginTop:15}}>
                    <Text style={{flex:2,
                        height:25,
                        paddingLeft:5,
                        paddingTop:5,
                        borderWidth:2,
                        borderColor:'#d9d9d9'
                      }}>
                      {this.state.selectedRadius}</Text>
                      <View style={{width:50}}>
                        <TouchableOpacity
                            style={pagestyles.buttonContainer}
                            underlayColor="rgba(0,0,0,0.0)"
                            onPress={() => { this.setState({visibleModal: 1}) } }
                          >
                          <Image style={{ width: 25, height: 25, resizeMode:'cover'}}
                            source={require("../../assets/icon_dropdown.png")} />
                        </TouchableOpacity>
                      </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>

    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  linerender: {
    width: "80%",
    paddingLeft:30,
    paddingRight:30
  },
  switchlabel: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    paddingTop: 15,
    fontSize:16
  },
  switchcontent: {
    marginTop: 10,
    marginBottom: 10
  },
  switchcoontainer: {
    flex:1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
