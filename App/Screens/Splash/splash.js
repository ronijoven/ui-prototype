import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image
} from 'react-native';

import styles from '../../Components/Styles';

let interval;

class Splash extends Component {

    componentDidMount() {
        const { navigate } = this.props.navigation;
        interval = setInterval(() => {
           navigate('FirstCode')
           clearInterval(interval);
        }, 2000);
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <View style={styles.upperbox}>
                <Image source={require('../../assets/logo.png')}
                  style={pagestyles.logo} />
                <Text>Bezig net laden...</Text>
            </View>
        );
    }
}

export default Splash;

const pagestyles = StyleSheet.create({
  logo: {
    height:150,
    width:150,
    margin:5,
    resizeMode: 'contain'
  },

});
