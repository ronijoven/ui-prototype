import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView
}  from 'react-native';
import {
  Text,  Button
} from 'react-native-elements';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class ProfileScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Add New Team',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_team.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props){
      super(props);
      this.state = {
        teamName: 'Week 25',
        teamAddress: 'Gemeente Rijswijk, 12345',
        teamContact: 'week25teammail@straat.info, 06123421323 ',
        teamChatname: 'QueenChat',
        teamPassword: '12345'
      };
  }

  render() {
    const { region } = this.props;

    return (
      <View style={{
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%'
        }}>
        <Banner title="Change Profiele Team"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Team') }
        />
        <ScrollView>
          <View style ={pagestyles.container}>
            <View style={{ marginTop:20, justifyContent:'center',alignItems:'center' }}>
              <Image source={require('../../assets/sample_pic.jpg')}
                style={pagestyles.avatar} />
            </View>
            <View style={{marginLeft:10, marginRight:10, marginTop:10}}>
              <Text style={{paddingTop:10}}>Naam</Text>
              <TextInput
                  style={pagestyles.textInputContainer}
                  value={this.state.teamName}
                  underlineColorAndroid="transparent"
                  multiline={false} />
              <View style={pagestyles.hr} />
              <Text style={{paddingTop:10}}>Adres</Text>
              <TextInput
                  style={pagestyles.textBoxInputContainer}
                  value={this.state.teamAddress}
                  underlineColorAndroid="transparent"
                  multiline={true} />
              <View style={pagestyles.hr} />
              <Text style={{paddingTop:10}}>Contactgegevens</Text>
              <TextInput
                  style={pagestyles.textBoxInputContainer}
                  value={this.state.teamContact}
                  underlineColorAndroid="transparent"
                  multiline={true} />
              <View style={pagestyles.hr} />
              <Text style={{paddingTop:10}}>Chatname</Text>
              <TextInput
                  style={pagestyles.textInputContainer}
                  value={this.state.teamChatname}
                  underlineColorAndroid="transparent"
                  multiline={false} />
              <View style={pagestyles.hr} />
              <Text style={{paddingTop:10}}>Watchwoord</Text>
              <TextInput
                  style={pagestyles.textInputContainer}
                  secureTextEntry={true}
                  placeholder="Voornaam"
                  value={this.state.teamPassword}
                  underlineColorAndroid="transparent"
                  multiline={false} />
                  <View style={{
                    width:'90%',
                    marginTop:20}}>
                    <Button
                      raised
                      disabled={this.state.submitbtn}
                      color='#fff'
                      disabledStyle={{
                        backgroundColor:'#9c9c9c'
                      }}
                      buttonStyle={pagestyles.btnSubmit}
                      title="WIJZIG MUN GEGEVENS"
                    />
                    <View style={pagestyles.halfHeight} />
                    <View style={pagestyles.halfHeight} />
                  </View>
                </View>

            </View>
          </ScrollView>
        </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    backgroundColor:'#f2f2f2',
    width:'100%',
    height: '100%',
    marginLeft:15,
    marginRight:15
  },
  TeamName: {
    fontSize: 24
  },
  HostName: {
    marginTop:10,
    fontSize: 18
  },
  TeamEmail: {
    fontSize: 12,
    marginTop:10,
  },
  hr: {
    width:'90%',
    marginTop:20,
    borderBottomColor: '#d0d0d0',
    borderBottomWidth: 2
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 75
  },
  btnSubmit: {
    backgroundColor:'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },
  textInputContainer : {
    width:'90%',
    marginTop:10,
    borderWidth: 1,
    borderColor: 'grey',
    padding: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    shadowColor: '#bdcadc',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    justifyContent: 'center'
  },
  textBoxInputContainer : {
    width:'90%',
    height:80,
    marginTop:10,
    borderWidth: 1,
    borderColor: 'grey',
    padding: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    shadowColor: '#bdcadc',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    justifyContent: 'center'
  },
  halfHeight: {
    height: 30,
    backgroundColor: 'transparent'
  }
});
