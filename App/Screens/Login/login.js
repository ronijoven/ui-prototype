import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Platform,
  Alert,
  NetInfo,
  AsyncStorage
} from 'react-native';
import {
  Text,  Button
} from 'react-native-elements';

//import ValidationComponent from 'react-native-form-validator';
//import LinearGradient from 'react-native-linear-gradient';
//import styles from '../Login/styles';

import AppData from '../../common/AppData';
import Api from '../../common/Api';
import ApiUtil from '../../common/ApiUtil';
//import BusyIndicator from 'react-native-busy-indicator';
//import loaderHandler from 'react-native-busy-indicator/LoaderHandler';

class Login extends Component { //ValidationComponent {

    constructor(props){
        super(props);

        this.state = {
            username : '',
            password : '',
        };
    }

    componentDidMount(){
    }

    render() {
        return (
            <ScrollView bounces={false}>
                <View style={pagestyles.container}>
                    <View style={pagestyles.upperboxContainer}>
                        <View style={pagestyles.upperbox}>
                            <Image source={require('../../assets/logo.png')}
                            style={pagestyles.logo} />
                        </View>
                        <View style={pagestyles.triangleContainer}>
                            <View style={pagestyles.triangle}></View>
                        </View>
                    </View>
                    <View style={pagestyles.forms}>
                        <Text style={pagestyles.inLoginTxt}>Inloggen</Text>
                        <View style={pagestyles.spacing}></View>
                        <View style={pagestyles.textInputContainer}>
                            <TextInput
                                style={{
                                    height: (Platform.OS === 'ios') ? 30 : 20,
                                }}
                                onChangeText={(text) => this.setState({username:text})}
                                underlineColorAndroid="transparent"
                                multiline={false}
                                placeholder="Email Address"/>
                        </View>
                        <View style={pagestyles.spacing}></View>
                        <View style={pagestyles.textInputContainer}>
                            <TextInput
                                style={{
                                    height: (Platform.OS === 'ios') ? 30 : 20,
                                }}
                                secureTextEntry={true}
                                onChangeText={(text) => this.setState({password:text})}
                                underlineColorAndroid="transparent"
                                multiline={false}
                                placeholder="Password"/>
                        </View>
                    </View>

                    <View style={pagestyles.spacing}></View>
                    <View style={pagestyles.spacing}></View>
                    <View style={pagestyles.buttonContainer}>
                    <Button
                      disabled={this.state.submitbtn}
                      disabledStyle={{
                        backgroundColor:'#9c9c9c'
                      }}
                      onPress={() => {
                        this.props.navigation.navigate('Dashboard')
                      }}
                      buttonStyle={pagestyles.btnSubmit}
                      title="IN LOGGEN"
                    />
                    </View>
                    <View style={pagestyles.spacing}></View>
                    <View style={pagestyles.spacing}></View>

                    <TouchableOpacity underlayColor="rgba(0,0,0,0.0)" onPress={() => this.props.navigation.navigate('Dashboard')}>
                        <Text style={{
                                color: '#96acc7',
                                justifyContent: 'center',
                                marginLeft:30,
                                marginRight:30,
                                textAlign: 'center'
                            }}>
                            Wachtwoord vergeten?
                        </Text>
                    </TouchableOpacity>
                    <View style={pagestyles.spacing}></View>
                    <View style={pagestyles.spacing}></View>
                    <TouchableOpacity underlayColor="rgba(0,0,0,0.0)"
                      onPress={() => this.props.navigation.navigate('Register')}>
                        <Text style={{
                                height:50,
                                color: '#96acc7',
                                justifyContent: 'center',
                                marginLeft:30,
                                marginRight:30,
                                textAlign: 'center'
                            }}>
                            Bent u een nieuwe gebruiker?
                        </Text>
                    </TouchableOpacity>
                    <View style={pagestyles.spacing}></View>
                    <View style={pagestyles.spacing}></View>
                </View>
            </ScrollView>
        );
    }
}

export default Login;

const pagestyles = StyleSheet.create({

    linearGradient: {
        height: 50,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 10,
        color: '#ffffff',
        backgroundColor: 'transparent',
    },
    buttonContainer:{
        height: 50,
        justifyContent: 'center',
        marginLeft:30,
        marginRight:30,
    },
    button : {
        height:50
    },
    logo: {
      height:100,
      width:100,
      margin:5,
      resizeMode: 'contain'
    },
    inLoginTxt : {
        fontSize : 20,
        color : "#6e85a1"
    },
    spacing: {
        height:20,
        backgroundColor: 'transparent'
    },
    textInputContainer : {
        padding: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#fff',
        shadowColor: '#bdcadc',
        shadowOffset: {
          width: 0,
          height: 1
        },
        shadowRadius: 5,
        shadowOpacity: 1.0
    },
    container : {
        flexDirection: 'column',
    },
    btnSubmit: {
      backgroundColor:'#64a932',
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 10,
      height:40
    },
    forms : {
        height:150,
        marginLeft:30,
        marginRight:30,
        justifyContent: 'center'
    },
    upperboxContainer : {
        height:200,
    },
    upperbox : {
        height:150,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    triangleContainer : {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'transparent',
    },
    triangle: {
        width: 0,
        height: 50,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 50,
        borderRightWidth: 50,
        borderBottomWidth: 25,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'white',
        backgroundColor: 'transparent',
        transform: [
            {rotate: '180deg'}
          ]
      },
  });
