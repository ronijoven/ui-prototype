import React, { Component } from 'react';
import {
  Platform,
  Button,
  Text,
  View,
  Image,
  Modal,
  StyleSheet,
  ScrollView
}  from 'react-native';

import { SearchBar, SocialIcon } from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MapView from 'react-native-maps';
import Banner from '../../Components/Banner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Home',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_home.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  onRegionChange(region) {
    this.setState({ region });
  }

  constructor(props){
      super(props);

      this.state = {
      };
  }

  render() {

    return (
      <View style= {{  height:'100%'}}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 30 : 0, height:110,width: '100%' }} >
           <Banner title="Straat.info"
             onPress={ () => this.props.navigation.navigate('DrawerOpen')}
             onBack={ () => onBackPress(this.props,'Chat') } />
           <View style={{flex:1, flexDirection:'row', justifyContent:'space-around',height:30, backgroundColor: '#121ab1' }} >
              <View style={{ backgroundColor: 'transparent', width:'80%' }}>
                <SearchBar
                  containerStyle={{ backgroundColor: 'white', borderWidth:1, borderColor: 'white', marginBottom:5,borderRadius:5,  height:30}}
                  inputStyle={{ backgroundColor: 'transparent', marginTop:0, paddingTop:0,marginBottom:5}}
                  placeholderTextColor='grey'
                  placeholder='Zoek adres'
                  noIcon
                />
              </View>
              <View>
                  <Text style={{   color: 'white' }}> Annuleer</Text>
              </View>
           </View>
           <View style={{ zIndex:1, flex:1, flexDirection: 'row', justifyContent: 'center' }}>
            <Image source={require('../../assets/down_icon.png')}
                style={{ width:40,  height:10 }}/>
           </View>
        </View>

        <View style = {pagestyles.container} >
          <MapView
            style={ pagestyles.map }
            initialRegion={{
              latitude: 37.78825,
              longitude: -122.4324,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}
           />
        </View>
     </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    position: 'absolute',
    zIndex:0,
    top: 35,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    zIndex:0,
    top: 35,
    left: 0,
    right: 0,
    bottom: 0,
  },

  iomgbg: {
    backgroundColor: '#ccc',
    resizeMode : 'contain',
    marginTop:1,
    paddingTop:0,
    position: 'absolute',
    height:400,
    width:'100%',
    justifyContent: 'flex-start'
  },
  topbar: {
    backgroundColor: '#ccc',
    height:40,
  }
});
