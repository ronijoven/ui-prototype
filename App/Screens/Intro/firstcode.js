import React, { Component } from 'react';
import {
  View,
  Platform,
  StyleSheet
} from 'react-native';
import {
  Text,
  Button,
} from 'react-native-elements';
import styles from '../../Components/Styles';

class FirstCode extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
    }

    render() {
        return (
              <View style={pagestyles.container}>
                <Text style={
                  { textAlign: 'center', color:'black' }}
                >Deze App wekt op dit moment alleen met een toegangscode</Text>
                <View style={styles.spacing}></View>
                <Text style={pagestyles.textconfirm}>Heeft u een code?</Text>
                <View style={styles.spacing}></View>
                <View style={pagestyles.navline}>
                  <Button
                    onPress={() => this.props.navigation.navigate('CodeOk', {name: 'Enter Code',parent: 'FirstCode'})}
                    buttonStyle={styles.btnGreen}
                    title="Ja"
                  />
                  <Button
                    onPress={() => this.props.navigation.navigate('AskInform', {name: 'Confirm Inform',parent: 'FirstCode'})}
                    buttonStyle={styles.btnRed}
                    title="Nee"
                  />
                </View>
              </View>
        );
    }
}

export default FirstCode;

const pagestyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop:'50%'
  },
  textconfirm: {
    color: 'black'
  },
  navline: {
    flex:2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  upperbox : {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
  }
});
