import React, { Component } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Platform
} from 'react-native';
import {
  Text,
  Button,
} from 'react-native-elements';
import styles from '../../Components/Styles';

class CodeOk extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }

    render() {
        return (
           <View style={pagestyles.container}>
                <Text style={pagestyles.textconfirm}>Voeg uw code hier in:</Text>
                <View style={styles.spacing}></View>
                <TextInput
                    style={pagestyles.inputbox}
                    placeholder="Enter Code"
                    underlineColorAndroid="transparent"
                    multiline={false}
                />
                <View style={styles.spacing}></View>
                <View style={pagestyles.navline}>
                  <Button
                    onPress={() => this.props.navigation.navigate('FirstCode', {name: 'Back'})}
                    buttonStyle={styles.btnRed}
                    title="Terug"
                  />
                  <Button
                    onPress={() => this.props.navigation.navigate('Login', {name: 'Login',parent: 'FirstCode'})}
                    buttonStyle={styles.btnBlue}
                    title="Ok"
                  />
                </View>
            </View>
        );
    }
}

export default CodeOk;

const pagestyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:'50%'
  },
  textconfirm: {
    color: 'black'
  },
  navline: {
    flex:2,
    flexDirection: 'row',
  },
  inputbox : {
    borderColor: 'grey',
    borderWidth: 1,
    width:200,
    paddingLeft:5,
    borderRadius: 10,
    height:40,
    shadowColor: "black",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0,
    },
  }
});
