import React, { Component } from 'react';
import {
  View,
  Platform,
  StyleSheet
} from 'react-native';
import {
  Text,
  Button,
} from 'react-native-elements';
import styles from '../../Components/Styles';

class AskInform extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
    }

    render() {
        return (
              <View style={pagestyles.container}>
                <Text style={
                  { textAlign: 'center', color:'black' }}
                >Op dit moment heeft u geen toegang. U kunt uw code gratis bij uw gemeente opvragen; vraagt u dan naar de toegangscode voor straat.info. Wilt u op de hoogte gehouden wilt worden van toekomstige ontwikkelingen? </Text>
                <View style={styles.spacing}></View>
                <View style={pagestyles.navline}>
                  <Button
                    onPress={() => this.props.navigation.navigate('Inform', {name: 'Enter Code',parent: 'AskInform'})}
                    buttonStyle={styles.btnGreen}
                    title="Ja"
                  />
                  <Button
                    onPress={() => this.props.navigation.navigate('CloseApp', {name: 'Close Application',parent: 'AskInform'})}
                    buttonStyle={styles.btnRed}
                    title="Nee"
                  />
                </View>
              </View>
        );
    }
}

export default AskInform;

const pagestyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop:'50%'
  },
  textconfirm: {
    color: 'black'
  },
  navline: {
    flex:2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  upperbox : {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
  }
});
