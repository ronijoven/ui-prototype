import React, { Component } from 'react';
import {
  View,
  TextInput,
  StyleSheet
} from 'react-native';

import {
  Text,
  Button,
} from 'react-native-elements';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import styles from '../../Components/Styles';


const sendform = (pass,state) => {
    //alert(JSON.stringify(pass));
    pass.navigation.navigate('Register', {name: 'Register'});
};

class Inform extends Component {

    constructor(props){
        super(props);
        this.state = {

        };
    }

    render() {

        return (
            <View style={pagestyles.container}>
                <View style={styles.spacing}></View>
                <Text style= {{
                   marginTop:20, fontSize: 22, textAlign: 'center', marginTop: 20
                }}>Hou mij op de hoogte</Text>
                <View style={styles.spacing}></View>
                <View style={pagestyles.inputContainer}>
                  <View style={pagestyles.iconContainer}>
                    <MaterialIcons
                    name='account-box'
                    size={24}
                    style={pagestyles.materialicon}
                    >
                    </MaterialIcons>
                  </View>
                  <TextInput
                      style={pagestyles.inputbox}
                      placeholder="Voornaam"
                      multiline={false}
                  />
                </View>
                <View style={pagestyles.inputContainer}>
                  <View style={pagestyles.iconContainer}>
                    <MaterialIcons
                    name='account-box'
                    size={24}
                    style={pagestyles.materialicon}
                    >
                    </MaterialIcons>
                  </View>
                  <TextInput
                      style={pagestyles.inputbox}
                      placeholder="Familienaam"
                      multiline={false}
                  />
                </View>
                <View style={pagestyles.inputContainer}>
                  <View style={pagestyles.iconContainer}>
                    <MaterialIcons
                    name='call'
                    size={24}
                    style={pagestyles.materialicon}
                    >
                    </MaterialIcons>
                  </View>
                  <TextInput
                      style={pagestyles.inputbox}
                      placeholder="Telefoonnummer"
                      underlineColorAndroid="transparent"
                      multiline={false}
                  />
                </View>
                <View style={pagestyles.inputContainer}>
                  <View style={pagestyles.iconContainer}>
                    <MaterialIcons
                    name='email'
                    size={24}
                    style={pagestyles.materialicon}
                    >
                    </MaterialIcons>
                  </View>
                  <TextInput
                      style={pagestyles.inputbox}
                      placeholder="Emailadres"
                      multiline={false}
                  />
                </View>
                <View style={pagestyles.inputContainer}>
                  <View style={pagestyles.iconContainer}>
                    <MaterialIcons
                    name='email'
                    size={24}
                    style={pagestyles.materialicon}
                    >
                    </MaterialIcons>
                  </View>
                  <TextInput
                      style={pagestyles.inputbox}
                      placeholder="Bevestigen Emailadres"
                      multiline={false}
                  />
                </View>
                <View style={
                  {width:'100%',
                  alignItems: 'center'}}>
                  <TextInput
                      style={pagestyles.textbox}
                      placeholder="Vul hier een eventuele boodschap in"
                      multiline={true}
                  />
                </View>
                <View style={styles.spacing}></View>
                <View style={pagestyles.navline}>
                  <Button
                    onPress={() => this.props.navigation.navigate('FirstCode', {name: 'Back'})}
                    buttonStyle={styles.btnRed}
                    title="Terug"
                  />
                  <Button
                    onPress={() => sendform(this.props,this.state) }
                    buttonStyle={styles.btnBlue}
                    title="Versturen"
                  />
                </View>
            </View>
        );
    }
}

export default Inform;

const pagestyles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  materialicon: {
      color: 'black',
      fontSize:40,
      paddingLeft:5,
      paddingRight:5,
      marginTop:5,
      marginBottom:5
  },
  inputContainer: {
      flex: 2,
      flexDirection: 'row',
  },
  iconContainer: {
      backgroundColor: '#ced6d8',
      height:50,
      borderRadius: 5,
      borderColor: '#dee5ea'
  },
  inputbox : {
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 10,
    width:'75%',
    marginTop:5,
    paddingLeft:10,
    paddingRight:5,
    height:45,
    shadowColor: "black",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
        height: 1,
        width: 0,
    },
  },
  textbox : {
    borderColor: 'grey',
    borderWidth: 1,
    width:'90%',
    paddingLeft:5,
    borderRadius: 10,
    height:90,
    shadowColor: "black",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
        height: 1,
        width: 0,
    },
  },
  navline: {
    flex:2,
    flexDirection: 'row',
  },

});
