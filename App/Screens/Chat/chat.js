import React, { Component } from 'react';
import {
  Platform,
  Button,
  Text,
  View,
  Image,
  Modal,
  StyleSheet,
  ScrollView
}  from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class ChatScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Home',
    drawerIcon: ({tintColor}) => {
      return (
        <MaterialIcons
        name='chat'
        size={24}
        style={{
          color: '#1733bf',
          paddingLeft: 10
        }}>
        </MaterialIcons>
      );
    }
  }

  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style={{
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%'
        }}>
        <Banner parent='Dashboard' title="Chat"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Home') }
        />
        <ScrollView>
          <View style ={pagestyles.container}>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
