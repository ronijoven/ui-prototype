import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  Picker,
  ScrollView
}  from 'react-native';

import {
  Text,
  Button
} from 'react-native-elements';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Modal from 'react-native-modal'
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class NewTeamScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Join Team',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_team.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  _renderButton = (text, onPress) => (
    <TouchableOpacity onPress={onPress}>
      <View style={{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 16,
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      }}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );

  _renderModalText = () => (
    <View style={pagestyles.modalContent}>
      <View style={{height:50}}>
        <Text>Team</Text>
        <View style={styles.hr}></View>
      </View>
      <Text>Uw aanwraag woor toegang tot het team ligt nu bij de team coordinator. U knijgt een bericht zodra u bent toeggelaten. Ondertussen kunt u de app gebruiken orn meldingen mee te doen.!</Text>
        {this._renderButton('Ok', () => this.setState({ visibleModal: false }))}
    </View>
  );

  constructor(props){
      super(props);
      this.state = { visibleModal: 1, vehicle: null };
  }

  render() {

    return (
      <ScrollView>
        <View style={{
            marginTop: Platform.OS === 'ios' ? 30 : 0,
            width: '100%',
            height:300,
            backgroundColor: 'white'
          }}>
          <Banner title="Join Team"
            onPress={ () => this.props.navigation.navigate('DrawerOpen')}
            onBack={ () => onBackPress(this.props,'Inform') }
          />
          <View style ={pagestyles.container}>

            <Modal
              isVisible={this.state.visibleModal === 1}
              animationIn={'slideInUp'}
              animationOut={'slideOutRight'}
              animationInTiming={1000}
              animationOutTiming={1000}
              backdropTransitionInTiming={1000}
              backdropTransitionOutTiming={1000}
            >
              {this._renderModalText()}
            </Modal>


          </View>

        </View>
      </ScrollView>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    width:'100%',
    marginTop:20,
    marginLeft:10,
    marginRight:10
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  picker: {

  }
});
