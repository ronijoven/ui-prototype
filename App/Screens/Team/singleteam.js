import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Picker,
  ScrollView
}  from 'react-native';

import {
  Text,
  Button,
  ButtonGroup,
  List,
  ListItem
} from 'react-native-elements';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';
import Modal from 'react-native-modal'

const options =["Select Team","Maak een keuze","Week 25","Mode 101","Concern"];

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

const list = [
  {
    name: 'Queen Band',
    icon: 'delete',
    subtitle: 'Vice President'
  },
  {
    name: 'King Kong X',
    icon: 'delete',
    subtitle: 'Vice Chairman'
  }
]

export default class TeamScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Mijn Team',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_team.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props){
      super(props);
      this.state = {
        teamName: 'Week 25',
        hostName: 'Gemeente Rijswijk',
        teamEmail: 'week25teammail@straat.info',
        selectedIndex: 2,
        visibleModal: 1

      };
      this.updateIndex = this.updateIndex.bind(this)
  }

  updateIndex (selectedIndex) {
    this.setState({selectedIndex})
  }

  render() {
    const buttons = [name='X', 'Ok']
    const { selectedIndex } = this.state

    const { region } = this.props;
    console.log(region);

    return (
      <View style={{
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%',
          height:'100%',
          backgroundColor: 'white'
        }}>
        <Banner title="Mijn Team"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Home') }
        />
        <ScrollView>
          <View style ={pagestyles.container}>
            <Modal
                isVisible={this.state.visibleModal === 1}
                animationIn={'slideInUp'}
                animationOut={'slideOutRight'}>
                <View style={pagestyles.modalContent}>
                    <View style={{height:50}}>
                      <Text>Please Select Team</Text>
                      <View style={styles.hr}></View>
                    </View>
                    <Picker
                      style={pagestyles.picker}
                      selectedValue={this.state.selectedTeam}
                      onValueChange={ (itemValue, itemIndex) => this.setState({selectedTeam: itemValue})}>
                      {options.map((item, index) => {
                          return (<Picker.Item label={item} value={item} key={index}/>)
                      })}
                    </Picker>
                    <Button
                      buttonStyle={styles.btnGreen}
                      onPress={() => this.setState({visibleModal: 0})}
                      title="Ok"
                    />
                 </View>
            </Modal>

            <View style={{ flex:1, flexDirection:'row' }}>
              <View style={{ justifyContent:'flex-start' }}>
                <Image source={require('../../assets/sample_pic.jpg')}
                  style={pagestyles.avatar} />
              </View>
              <View style={{ marginLeft:'5%'  }}>
                <Text style={pagestyles.TeamName}>{this.state.teamName}</Text>
                <Text style={pagestyles.HostName}>{this.state.hostName}</Text>
                <Text style={pagestyles.TeamEmail}>{this.state.teamEmail}</Text>
              </View>
            </View>
          </View>
          <View style = {{ marginTop:30, flex:1, flexDirection: 'row'}}>
            <View style = {{ width:'20%', marginLeft: 50 }}>
              <TouchableOpacity
                style={pagestyles.buttonContainer}
                underlayColor="rgba(0,0,0,0.0)"
                onPress={() => { this.props.navigation.navigate("NewTeam") } }
              >
              <Text style={{
                textAlign:'center',
                alignItems:'center'
              }}>Maak extra team aan</Text>
              </TouchableOpacity>
            </View>
            <View style = {{ width:'40%', marginLeft:'10%' }}>
              <TouchableOpacity
                  style={pagestyles.buttonContainer}
                  underlayColor="rgba(0,0,0,0.0)"
                  onPress={() => { this.props.navigation.navigate("ChangeTeam") } }
                >
                <Text style={{}}>Wijzig teamprofiel</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{
              marginTop:10,
            }}>
            <Image style={{
              marginLeft:'40%',
            }}
            source={require("../../assets/boxmarker.png")}/>
          </View>
          <View style ={pagestyles.boxcontainer}>
            <View style = {{marginTop:0}}>
                <Text style={{marginTop:10}}>Aanuraag nieuw leden</Text>
                <View style={pagestyles.hr}></View>

                <View style={{
                    marginTop:20,
                    marginBottom:30,
                    flex:2, flexDirection:'row'
                  }}>
                  <View style={{
                      justifyContent:'flex-start'
                    }}>
                    <Text style={{paddingTop:5}}>Testaa Aaa</Text>
                  </View>
                  <View style={{ marginRight:20, flex:2, justifyContent:'flex-end',alignItems:'flex-end'}}>
                    <View style={{ marginTop:-10}}>
                      <ButtonGroup
                        onPress={this.updateIndex}
                        selectedIndex={selectedIndex}
                        buttons={buttons}
                        containerStyle={{
                          width: 100,
                          height: 40,
                          borderRadius: 10,
                          borderColor: '#dadada',
                          backgroundColor: "#e5e5e5"
                        }}
                      />
                    </View>
                    </View>
                </View>
                <View style={pagestyles.hr}></View>
                <Text style={{marginTop:20}}>Huidige Leden</Text>
                <View style={{marginBottom:30}}>
                  <List
                    containerStyle={{
                        width:'95%',
                        height:50,
                        backgroundColor: 'transparent'
                      }}>
                    {
                      list.map((l, i) => (
                        <ListItem
                          key={i}
                          title={l.name}
                          rightIcon={{name: l.icon, }}
                        />
                      ))
                    }
                  </List>
                </View>
                <View style={{marginTop:150, width:'90%'}}>
                  <Button
                    buttonStyle={styles.btnGreen}
                    title="Vrienden Ultnodigen"
                  />
                </View>
            </View>
          </View>
          </ScrollView>
        </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    width:'100%',
    marginTop:20,
    marginLeft:10,
    marginRight:10
  },
  hr: {
    width:'90%',
    marginTop:20,
    borderBottomColor: '#d0d0d0',
    borderBottomWidth: 2
  },
  TeamName: {
    fontSize: 24
  },
  HostName: {
    marginTop:10,
    fontSize: 18
  },
  TeamEmail: {
    fontSize: 12,
    marginTop:10,
  },
  boxcontainer: {
    width:'100%',
    marginLeft:20,
    backgroundColor:"transparent",//#f2f2f2",
    flex:1,
    height:450,
    marginTop:0,
    //flexDirection:'row',
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  separator: {
    height: 1,
    width: "86%",
    backgroundColor: "#CED0CE",
    marginLeft: "14%"
  },
  flexcontainer: {
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 75
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
