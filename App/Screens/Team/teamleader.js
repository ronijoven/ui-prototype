import React, { Component } from 'react';
import {
  Platform,
  Button,
  Text,
  View,
  Image,
  Modal,
  StyleSheet,
  TouchableOpacity,
  ScrollView
}  from 'react-native';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class TeamLeaderScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Mijn Team',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_team.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props){
      super(props);
      this.state = {
        teamName: 'Week 25',
        hostName: 'Gemeente Rijswijk',
        teamEmail: 'week25teammail@straat.info'
      };
  }

  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <ScrollView>
        <View style={{
            marginTop: Platform.OS === 'ios' ? 30 : 0,
            width: '100%'
          }}>
          <Banner title="Mijn Team"
            onPress={ () => this.props.navigation.navigate('DrawerOpen')}
            onBack={ () => onBackPress(this.props,'Home') }
          />
          <View style ={pagestyles.container}>
            <View style={{ flex:1, flexDirection:'row' }}>
              <View style={{ justifyContent:'flex-start' }}>
                <Image source={require('../../assets/sample_pic.jpg')}
                  style={pagestyles.avatar} />
              </View>
              <View style={{ marginLeft:'45%', position: 'absolute'  }}>
                <Text style={pagestyles.TeamName}>{this.state.teamName}</Text>
                <Text style={pagestyles.HostName}>{this.state.hostName}</Text>
                <Text style={pagestyles.TeamEmail}>{this.state.teamEmail}</Text>
              </View>
            </View>
          </View>
          <View style = {{ marginTop:170, flex:1, flexDirection: 'row'}}>
            <View style = {{ width:'20%', marginLeft: 50 }}>
              <Text style={{
                textAlign:'center',
                alignItems:'center'
              }}>Maak extra team aan</Text>
            </View>
            <View style = {{ width:'40%', marginLeft:'10%' }}>
              <TouchableOpacity
                style={pagestyles.buttonContainer}
                underlayColor="rgba(0,0,0,0.0)"
                onPress={() => { this.props.navigation.navigate("Login") } }
              >
                <Text style={{}}>Wijzig teamprofiel</Text>
              </TouchableOpacity>
            </View>


          </View>
          <View style ={pagestyles.boxcontainer}>
            <Image source={require("../../assets/boxmarker.png")}/>
            <View style = {{}}>
              <Text style={{}}>Aanuraag nieuw leden</Text>
            </View>

          </View>

        </View>
      </ScrollView>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    position:'absolute',
    width:'100%',
    marginTop:50,
    marginLeft:10,
    marginRight:10
  },
  TeamName: {
    fontSize: 24
  },
  HostName: {
    marginTop:10,
    fontSize: 18
  },
  TeamEmail: {
    fontSize: 12,
    marginTop:10,
  },
  boxcontainer: {
    position:'absolute',
    width:'100%',
    backgroundColor:"#f2f2f2",
    flex:1,
    height:450,
    marginTop:270,
    flexDirection:'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  flexcontainer: {
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  avatar: {
    width: 150,
    height: 150,
    borderRadius: 75
  }
});
