import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView
}  from 'react-native';
import {
  Text,  Button
} from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';
import Picker from 'react-native-picker';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class ChangeTeamScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Add New Team',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_team.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props){
      super(props);
      this.state = {
        teamName: 'Week 25',
        teamHost: 'Gemeente Rijswijk',
        teamEmail: 'week25teammail@straat.info',
        submitbtn: true
      };
  }

  render() {
    const { region } = this.props;

    return (
      <View style={{
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%'
        }}>
        <Banner title="Mijn Team"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Team') }
        />
        <ScrollView>
          <View style={{
              marginLeft:10, marginRight:10, marginTop:10
              }}>
              <View style={{marginBottom:20,flex:1,flexDirection:'row'}}>
                <Text style={{width:'30%'}}>Gemeente:</Text>
                <TextInput
                    style={pagestyles.textInputContainer}
                    value={this.state.teamHost}
                    underlineColorAndroid="transparent"
                    multiline={false} />
              </View>
              <View style={{marginBottom:20,flex:1,flexDirection:'row'}}>
                <Text style={{width:'30%'}}>Team Naam:</Text>
                <TextInput
                    style={pagestyles.textInputContainer}
                    value={this.state.teamName}
                    underlineColorAndroid="transparent"
                    multiline={false} />
              </View>
              <View style={{marginBottom:50,flex:1,flexDirection:'row'}}>
                <Text style={{width:'30%'}}>Team e-mailadres:</Text>
                <TextInput
                    style={pagestyles.textInputContainer}
                    value={this.state.teamEmail}
                    underlineColorAndroid="transparent"
                    multiline={false} />

              </View>
              <View style ={{}}>
                <View style={{ marginBottom:20,justifyContent:'center',alignItems:'center' }}>
                  <Image source={require('../../assets/sample_pic.jpg')}
                    style={pagestyles.avatar} />
                </View>
              </View>
              <View style={{flex:1,flexDirection:'column',marginTop:20,marginBottom:100, paddingBottom:100}}>
                <Button
                  disabled={this.state.submitbtn}
                  disabledStyle={{
                    backgroundColor:'#9c9c9c'
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('Team')
                  }}
                  buttonStyle={pagestyles.btnSubmit}
                  title="Browse"
                />
                <View style={pagestyles.halfHeight} />
                <View style={pagestyles.halfHeight} />
              </View>
           </View>
         </ScrollView>
      </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    width:'100%',
    marginLeft:20,
    marginRight:20
  },
  avatar: {
    width: 250,
    height: 250,
    borderRadius: 125
  },
  btnSubmit: {
    backgroundColor:'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },
  textInputContainer : {
    flex:1,
    height: Platform.OS === 'ios' ? 40 : 30,
    width:'65%',
    borderWidth: 1,
    borderColor: 'grey',
    padding: 5,
    backgroundColor: 'white',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    shadowColor: '#bdcadc',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 5,
    shadowOpacity: 1.0,
    justifyContent: 'center'
  },
  halfHeight: {
    height: 30,
    backgroundColor: 'transparent'
  }
});
