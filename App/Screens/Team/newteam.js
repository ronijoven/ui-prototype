import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  Modal,
  Alert,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView
}  from 'react-native';
import {
  Text,  Button
} from 'react-native-elements';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class NewTeamScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Add New Team',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_team.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props){
      super(props);
      this.state = {
        teamName: 'Week 25',
        hostName: 'Gemeente Rijswijk',
        teamEmail: 'week25teammail@straat.info',
        submitbtn: true
      };
      //Alert.alert(JSON.stringify(props));
  }

  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style={{
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%'
        }}>
        <Banner title="Add New Team"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Team') }
        />
        <ScrollView>
          <View style ={pagestyles.container}>
            <View style = {{ marginLeft: 10 }}>
              <Text style={{fontSize:24
              }}>Een nieuw team starten</Text>
            </View>

            <View style = {{ marginTop:10,
              flex:1, flexDirection: 'column',
              alignItems:'center',justifyContent:'center'}}>
              <View style={{
                  width:'80%',
                  marginTop:20
                }}>
                <TextInput
                    style={pagestyles.textInputContainer}
                    value={this.state.teamHost}
                    underlineColorAndroid="transparent"
                    placeholder='Naam team'
                    multiline={false} />
              </View>
              <View style={{
                width:'80%',
                marginTop:20}}>
                <TextInput
                    style={pagestyles.textInputContainer}
                    value={this.state.teamHost}
                    underlineColorAndroid="transparent"
                    placeholder='Team e-mailadres'
                    multiline={false} />
              </View>
              <View style={{
                width:'90%',
                marginTop:20}}>
                <Button
                  raised
                  color='#4d5a67'
                  disabledStyle={{
                    backgroundColor:'#9c9c9c'
                  }}
                  onPress={() => {
                    this.setState({submitbtn:false})
                  }}
                  buttonStyle={pagestyles.btnSelect}
                  title="Selecteer teamlogo"
                />
              </View>
              <View style={{height:200}}></View>
              <View style={{
                width:'90%',
                marginTop:20}}>
                <Button
                  raised
                  disabled={this.state.submitbtn}
                  color='#4d5a67'
                  disabledStyle={{
                    backgroundColor:'#9c9c9c'
                  }}
                  onPress={() => {
                    this.props.navigation.navigate('Team')
                  }}
                  buttonStyle={pagestyles.btnSubmit}
                  title="REGISTREREN"
                />
              </View>

            </View>
          </View>
          </ScrollView>
        </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    width:'100%',
    marginTop:50,
    marginLeft:10,
    marginRight:10
  },

  textInputContainer : {
    height: 40,
    borderWidth: 1,
    borderColor: 'grey',
    padding: 5,
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    shadowColor: '#bdcadc',
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 5,
    shadowOpacity: 1.0
  },

  btnSelect: {
    backgroundColor:'#eaeaea',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor:'#afb4bb',
    borderWidth:2,
    borderRadius: 10,
    height:40
  },
  btnSubmit: {
    backgroundColor:'#34e82c',
    alignItems: 'center',
    justifyContent: 'center',
    borderColor:'#afb4bb',
    borderWidth:2,
    borderRadius: 10,
    height:40
  },
});
