import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  Modal,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  ScrollView
}  from 'react-native';

import {
  Text,
  Button,
  Icon,
  List,
  ListItem
} from 'react-native-elements';
import renderIf from 'render-if';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};
const list = [
  {
    name: 'Nieuw Here',
    icon: 'delete',
    address1: 'Pr. Beautrixlaa 10, 2285 VZ',
    address2: 'Rijsqijk, Nederland',
    date: '2017-06-17 on 21:19:05',
    marked: 'Y'
  },
  {
    name: 'Nieuw',
    icon: 'delete',
    address1: 'Willem Royaardsstraat 3-A',
    address2: '2285 Rijsqijk, Nederland',
    date: '2017-06-17 on 21:19:05',
    marked: 'N'
  },
  {
    name: 'Nieuw',
    icon: 'delete',
    address1: 'Minister van Houtenlaan 1',
    address2: '2285 EA Rijsqijk, Nederland',
    date: '2017-06-17 on 21:19:05',
    marked: 'N'
  },
  {
    name: 'Nieuw',
    icon: 'delete',
    address1: 'Houtenlaan 2',
    address2: 'Rijsqijk, Nederland',
    date: '2017-06-17 on 21:19:05',
    marked: 'N'
  },
  {
    name: 'Nieuw',
    icon: 'delete',
    address1: 'Houtenlaan 2',
    address2: 'Rijsqijk, Nederland',
    date: '2017-06-17 on 21:19:05',
    marked: 'N'
  }
]

export default class ReportsScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Nijb meldingen',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_reports.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props) {
    super(props);
    this.state = {
        dataSource: list
    };

  }

  _onPress = () => { this.props.onPressItem(this.props.id); };


  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style={{
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%'
        }}>
        <Banner parent='Home' title="Nijb meldingen"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Home') }
        />
        <ScrollView>
          <View style ={pagestyles.container}>
            <View style={{
                flex:1,
                flexDirection:'row',
                marginBottom:30,
                marginLeft:20,
                marginRight:20
              }}>
              <FlatList
                data={this.state.dataSource}
                renderItem={
                  ({item}) =>
                  <View>
                    <View style={pagestyles.hr}/>
                    <View style={{flex:2,width:'90%',marginTop:20,flexDirection:'row'}}>
                      <View style={{justifyContent:'flex-start',alignItems:'flex-start'}}>
                        <Text style={{fontSize:18}}>{item.name}</Text>
                      </View>
                      <View style={{flex:2,flexDirection:'row', justifyContent:'flex-end',alignItems:'flex-end'}}>
                        <Icon name={item.icon} />
                      </View>
                    </View>
                    {renderIf(item.marked=='Y')(
                      <View style = {pagestyles.overlay}>
                        <TouchableOpacity
                            onPress={() => { this.props.navigation.navigate("ReportDetail") } }
                          >
                            <Image source = {require('../../assets/report_marker.png')} />
                        </TouchableOpacity>
                      </View>

                    )}
                    <Text style={{fontSize:14}}>{item.address1}</Text>
                    <Text style={{fontSize:14}}>{item.address2}</Text>
                    <Text style={{fontSize:14}}>{item.date}</Text>
                  </View>
                } keyExtractor={(item, index) => index}

              />
            </View>
          </View>
          </ScrollView>
        </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    backgroundColor:'white',
    height: '100%',
    width: 400
  },
  hr: {
    width:'90%',
    marginTop:20,
    borderBottomColor: '#d0d0d0',
    borderBottomWidth: 2
  },
  overlay: {
    position:'absolute',
    zIndex: 2,
    marginTop:10,
    marginLeft:10,
    opacity: 0.5,
    backgroundColor: 'transparent'
  }
});
