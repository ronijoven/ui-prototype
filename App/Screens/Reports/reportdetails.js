import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  Modal,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  ScrollView
}  from 'react-native';

import {
  Text,
  Button,
  Icon,
  List,
  ListItem
} from 'react-native-elements';
import renderIf from 'render-if';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/ExpandedBanner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};
const list = [
  {
    name: 'afval',
    date: '2017-06-17',
    loc: 'BEKLIK ELDING',
    pic: require('../../assets/report_img1.png'),
    marked: 'Y'
  },
  {
    name: 'groen',
    date: '2017-06-17',
    loc: 'BEKLIK ELDING',
    pic: require('../../assets/report_img2.png'),
    marked: 'Y'
  },
  {
    name: 'speeLplaats',
    date: '2017-06-17',
    loc: 'BEKLIK ELDING',
    pic: require('../../assets/report_img3.png'),
    marked: 'Y'
  },
  {
    name: 'plaats',
    date: '2017-06-17',
    loc: 'BEKLIK ELDING',
    pic: require('../../assets/report_img1.png'),
    marked: 'N'
  },
  {
    name: 'speeL',
    date: '2017-06-17',
    loc: 'BEKLIK ELDING',
    pic: require('../../assets/report_img3.png'),
    marked: 'N'
  }
]

export default class ReportsScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Nijb meldingen',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_reports.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props) {
    super(props);
    this.state = {
        dataSource: list
    };

  }

  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <View style= {{  height:'100%'}}>
        <View style={{ marginTop: Platform.OS === 'ios' ? 30 : 0, height:110,width: '100%' }} >
            <Banner parent='Reports' title="Straat.info"
              onPress={ () => this.props.navigation.navigate('DrawerOpen')}
              onBack={ () => onBackPress(this.props,'Home') }
            />
        </View>

          <ScrollView>
            <View style ={pagestyles.container}>
              <View style={{ flex:1, flexDirection:'row',
                  marginBottom:30, marginLeft:20, marginRight:20 }}>
                <FlatList
                  data={this.state.dataSource}
                  renderItem={
                    ({item}) =>
                    <View style={{width:'100%',marginLeft:10 }}>
                      <View style={{flex:2,width:'100%',marginTop:20,flexDirection:'row'}}>
                        <View style={{paddingLeft:15,backgroundColor:'white',width:'90%',justifyContent:'flex-start',alignItems:'flex-start'}}>
                          <Text style={pagestyles.itemColumn}>{item.name}</Text>
                          <Text style={pagestyles.itemColumn}>{item.date}</Text>
                          <Text style={pagestyles.itemColumnRed}>{item.loc}</Text>
                        </View>

                        <View style={{ justifyContent:'flex-end',alignItems:'flex-end'}}>
                          {renderIf(item.marked=='Y')(
                            <View style = {pagestyles.overlay}>
                                <Image style={pagestyles.imageItem} source = {require('../../assets/info_normal.png')} />
                            </View>
                          )}
                          {renderIf(item.marked=='N')(
                            <View style = {pagestyles.overlay}>
                                <Image style={pagestyles.imageItem} source = {require('../../assets/info_warn.png')} />
                            </View>
                          )}
                          <Image style={pagestyles.imageItem}
                            source = { item.pic } />
                        </View>
                      </View>
                      <View style={{flex:2,flexDirection:'row',marginTop:10,paddingLeft:10}}>
                        <Image source = {require('../../assets/post_icon.png')} />
                        <Text style={{fontSize:14, marginLeft:5}}>brericten</Text>
                        <Text style={{fontSize:14, marginLeft:5,color:'#8096b8'}}>Toon</Text>
                      </View>
                    </View>
                  } keyExtractor={(item, index) => index}

                />
              </View>
            </View>
          </ScrollView>
       </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    backgroundColor: '#f2f2f2',
    height: '100%',
    width: '100%'
  },
  hr: {
    width:'90%',
    marginTop:20,
    borderBottomColor: '#d0d0d0',
    borderBottomWidth: 2
  },
  overlay: {
    position:'absolute',
    zIndex: 2,
    opacity: 0.5,
    backgroundColor: 'transparent',
  },
  itemColumn: {
    fontSize:18,
    paddingBottom:10
  },
  itemColumnRed: {
    color:'red',
    fontSize:18,
    fontWeight: 'bold'
  },
  imageItem: {
    height:100,
    width: 90,
    resizeMode:'contain'
  }
});
