import React, { Component } from 'react';
import {
  View,
  ScrollView,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  Platform
} from 'react-native';

import {
  Text,  Button, CheckBox
} from 'react-native-elements';

//import LinearGradient from 'react-native-linear-gradient';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import styles from '../../Components/Styles';

const submitPress = (pass,module) => {
  //navigate('Inform', {name: 'Request Code',parent: 'FirstCode'})
}

const setBtnSubmit = (pass,state) => {
   state.accepted = true;
   state.submitbtn = false;
   //this.setState({ errors: errors });
   setState({ submitbtn : false });
   return;
}

class RegistrationStepOne extends Component {

  constructor(props){
      super(props);
      this.state = {
        accepted: false,
        submitbtn: true,
        types1: [
          {label: 'Dhr.', value: 0},
          {label: 'Mevr.', value: 1}
        ],
        value1: 0,
        value1Index: 0
      };
  }
  static navigationOptions = ({ navigation }) => {
    tabBarLabel: 'Gegevens'
  }

//    header: (/
//       <View style={{
//           height:80,
//           marginTop:20
//          }}>
//          <Text>This is my HEAER</Text>
//        </View>
//      )
//  }


  render() {

    return (
      <ScrollView>
      <View style={pagestyles.container}>
        <View style={{
          flex:1,
          flexDirection:'row',
          justifyContent: 'flex-start',
          alignItems: 'flex-end',
        }}>
          <Image stuyle={{
            resizeMode: 'cover'
          }} source={require("../../assets/tabmarker.png")}/>
        </View>
        <View style={pagestyles.box}>
          <Text style={{ marginTop:20, fontSize:24, color: '#6e85a1' }}>Uw contactgegevens</Text>
          <View style={pagestyles.radioButtonContainer}>
            <RadioForm
                ref="radioForm"
                radio_props={this.state.types1}
                initial={0}
                formHorizontal={true}
                labelHorizontal={true}
                buttonColor={'#bdcadc'}
                labelColor={'#bdcadc'}
                buttonSize={10}
                buttonWrapStyle={{marginLeft: 100}}
                buttonOuterSize={20}
                labelWrapStyle={{marginLeft: 100}}
                animation={false}
                onPress={(value, index) => {
                  this.setState({
                    value1:value,
                    value1Index:index
                  });
                  if(index === 0){
                  }else{
                  }
                }}
              />
          </View>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={pagestyles.textInputContainer}>
              <TextInput
                  style={{ height: (Platform.OS === 'ios') ? 30 : 40 ,flex:1}}
                  placeholder="Voornaam"
                  underlineColorAndroid="transparent"
                  multiline={false} />
          </View>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={pagestyles.textInputContainer}>
              <TextInput
                  style={{ height: (Platform.OS === 'ios') ? 30 : 40 , flex:1}}
                  placeholder="Achternaam"
                  underlineColorAndroid="transparent"
                  multiline={false}/>
          </View>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={{ height: 30, justifyContent: 'center' }}>
              <View style={{ height: 1 , backgroundColor:'#6e85a1' }}></View>
          </View>
          <View style={pagestyles.textInputContainer}>
              <TextInput
                  style={{ height: (Platform.OS === 'ios') ? 30 : 40 , flex:1}}
                  placeholder="Gebruikersnaam "
                  underlineColorAndroid="transparent"
                  multiline={false}/>
          </View>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={{ height: 30, justifyContent: 'center' }}>
              <View style={{ height: 1 , backgroundColor:'#6e85a1' }}></View>
          </View>
          <View style={pagestyles.horizontalContainer}>
          <View style={[pagestyles.textInputContainer,{flex:2}]}>
              <TextInput
                  style={{ height: (Platform.OS === 'ios') ? 30 : 40, flex:1}}
                  placeholder="Adres"
                  underlineColorAndroid="transparent"
                  multiline={false}/>
          </View>
          <View style={pagestyles.horizontalSpacing}/>
          <View style={[pagestyles.textInputContainer,{flex:1}]}>
              <TextInput
                  style={{ height: (Platform.OS === 'ios') ? 30 : 40 , flex:1 }}
                  placeholder="Nr."
                  underlineColorAndroid="transparent"
                  multiline={false}/>
          </View>
        </View>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={pagestyles.horizontalContainer}>
            <View style={pagestyles.textInputContainer}>
                <TextInput
                    style={{ height: (Platform.OS === 'ios') ? 30 : 40 }}
                    placeholder="Achternaam"
                    underlineColorAndroid="transparent"
                    multiline={false}/>
            </View>
            <View style={pagestyles.horizontalSpacing}/>
            <View style={pagestyles.textInputContainer}>
                <TextInput
                    style={{ height: (Platform.OS === 'ios') ? 30 : 40  }}
                    placeholder="Plaats"
                    underlineColorAndroid="transparent"
                    multiline={false}/>
            </View>
          </View>
          <View style={{ height: (Platform.OS === 'ios') ? 30 : 40 , justifyContent: 'center' }}>
              <View style={{ height: 1 , backgroundColor:'#6e85a1' }}></View>
          </View>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={pagestyles.textInputContainer}>
              <TextInput
                  style={{ height: (Platform.OS === 'ios') ? 30 : 40 , flex:1 }}
                  placeholder="E-mailadres"
                  underlineColorAndroid="transparent"
                  multiline={false}/>
          </View>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={pagestyles.textInputContainer}>
              <TextInput
                  style={{ height: (Platform.OS === 'ios') ? 30 : 40 , flex:1 }}
                  placeholder="Mobiel telefoonnummer"
                  underlineColorAndroid="transparent"
                  multiline={false}/>
          </View>
          <View style={{ height: 30 , justifyContent: 'center' }}>
              <View style={{ height: 1 , backgroundColor:'#6e85a1' }}></View>
          </View>
          <View style={pagestyles.textInputContainer}>
              <TextInput
                  style={{ height: (Platform.OS === 'ios') ? 30 : 40 , flex:1 }}
                  placeholder="Kies wachtwoord"
                  underlineColorAndroid="transparent"
                  multiline={false}/>
          </View>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={pagestyles.verticalFieldsSpacing}/>
          <View style={pagestyles.verticalFieldsSpacing}/>

          <CheckBox
            center
            title='Ik accepteer hierbij de algemene voorwaarden'
            iconLeft
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checked={this.state.accepted}
            onPress={() => {
              this.setState({
                submitbtn:false,
                accepted:true
              });
            }}
          />
          <Button
            disabled={this.state.submitbtn}
            disabledStyle={{
              backgroundColor:'#9c9c9c'
            }}
            onPress={() => {
              this.props.navigation.navigate('TabTwo')
            }}
            buttonStyle={pagestyles.btnSubmit}
            title="VOLGENDE STAP"
          />
          <TouchableOpacity
            style={pagestyles.buttonContainer}
            underlayColor="rgba(0,0,0,0.0)"
            onPress={() => { this.props.navigation.navigate("Login") } }
          >
            <Text style={pagestyles.buttonText}>{"Bent u een Bestaande Gebruiker?"}</Text>
          </TouchableOpacity>
        </View>
      </View>
      </ScrollView>
    )
  }
}

export default RegistrationStepOne;

const pagestyles = StyleSheet.create({
  container: {
      flex:1,
      marginTop:30,
      marginLeft:0,
      marginRight:0,
      flexDirection: 'column',
      left: 0,
      right: 0,
      width:'100%',
      backgroundColor: '#f2f2f2'
  },
  box: {
      paddingLeft:20,
      paddingRight:20
  },
  radioButtonContainer : {
      marginTop:10,
  },
  horizontalSpacing: {
      width:10,
      backgroundColor: 'transparent'
  },
  verticalSpacing: {
      height:20,
      backgroundColor: 'transparent'
  },
  verticalFieldsSpacing: {
      height:10,
      backgroundColor: 'transparent'
  },
  textInputContainer : {
      padding: 5,
      backgroundColor: '#FFFFFF',
      borderRadius: 5,
      borderWidth: 1,
      borderColor: '#fff',
      shadowColor: '#bdcadc',
      shadowOffset: { width: 0, height: 1 },
      shadowRadius: 5,
      shadowOpacity: 1.0,
      justifyContent: 'center',
      flexDirection: 'row',
  },
  horizontalContainer : {
      flexDirection: 'row',
  },
  buttonContainer:{
      height: 50,
      justifyContent: 'center',
  },
  textInput : {
      textAlign: 'right'
  },
  buttonText: {
      fontSize: 18,
      fontFamily: 'Gill Sans',
      textAlign: 'center',
      margin: 10,
      color: 'blue',
      backgroundColor: 'transparent',
  },
  //btnSubmit: {
      //backgroundColor: '#64a932'
  //},
  btnSubmit: {
    backgroundColor:'green',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },
  btnGray: {
    backgroundColor: '#f2f2f2'
  },
  btnGrey: {
    backgroundColor:'#9c9c9c',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },

});
