import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Picker,
  NetInfo,
  Alert
} from 'react-native';

import {
  Text,  Button, CheckBox
} from 'react-native-elements';

//import LinearGradient from 'react-native-linear-gradient';
import renderIf from 'render-if';
import Radio from 'radio-button-react-native';
import Api from '../../common/Api';
import ApiUtil from '../../common/ApiUtil';
import Modal from 'react-native-modal'
import styles from '../../Components/Styles';

/* for select oicker data */
const options =["Select Team","Maak een keuze","Week 25","Mode 101","Concern"];

export default class RegistrationStepThree extends React.Component {

  static navigationOptions = {
    tabBarLabel: 'Team'
  }

  constructor(props){
    super(props)
    this.state = {
      accepted: false,
      submitbtn: true,
      teamList:[],
      value: 0,
      showselect: false,
      visibleModal: 0,
      selectedTeam: 'Select Team'
    };
  }

  getTeam(){
    NetInfo.isConnected.fetch().then(isConnected => {
      if(isConnected){
          Api.getTeam('','','',1,'')
              .then(getTeamResult => {
                  if(getTeamResult.status === 1){
                  }else{
                      Alert.alert(
                          'Error',
                          ApiUtil.serverResponseResult(loginResult.result),[{text: 'OK', onPress: () => console.log('OK Pressed')}],
                          { cancelable: false }
                      );
                  }

              }).catch(loginError => {
                  Alert.alert(
                      'Error',
                      'Server Error',[{text: 'OK', onPress: () => console.log('OK Pressed')}],
                      { cancelable: false }
                  );
              });
      }else{
          Alert.alert(
              'Error',
              'No Internet Connection. Please try again.',[{text: 'OK', onPress: () => console.log('OK Pressed')}],
              { cancelable: false }
          );
      }
    });
  }

  handleOnPress(value){
      this.setState({value:value})
      if (JSON.stringify(value)==1) {
        this.setState({showselect:true})
      } else {
        this.setState({showselect:false})
      }
  }

  handleSubmit(that) {
    //Alert.alert(JSON.stringify(value));
    //Alert.alert(JSON.stringify(state));
    if (that.state.value == 0) {  // navigate to new team
      that.props.navigation.navigate('NewTeam');
    } else if (that.state.value == 1) {
      that.props.navigation.navigate('ConfirmTeam')
    }
  }

  render() {
    return (
      <View style={pagestyles.container}>
        <View style={{
            flex:1,
            height:30,
            backgroundColor:'white',
            flexDirection:'row',
            justifyContent: 'flex-end',
          }}>
          <Image stuyle={{
              resizeMode: 'cover'
            }} source={require("../../assets/tabmarker.png")}/>
        </View>
        <View style={pagestyles.box}>
            <Text style={{
                marginTop: 50,
                fontSize:24,
                textAlign:'center',
                alignItems:'center',
                color: '#6e85a1'
            }}>Maak een keuze uit onderstande</Text>

            <View style={pagestyles.radioButtonContainer}>
              <Radio innerCircleColor={"blue"} currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this) }>
                <Text style={pagestyles.radioButtonText}>Ik ben een individuele melder</Text>
              </Radio>
              <Radio innerCircleColor={"blue"} currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}>
                <Text style={pagestyles.radioButtonText}>Ik toegang tot straat.info van mijn bpt</Text>
              </Radio>

              {renderIf(this.state.showselect)(
                <TouchableOpacity
                    style={pagestyles.buttonContainer}
                    underlayColor="rgba(0,0,0,0.0)"
                    onPress={() => { this.setState({visibleModal: 1}) } }
                  >
                  <Text style={{ marginTop: 5}}>
                    {this.state.selectedTeam}
                  </Text>
                </TouchableOpacity>
              )}

              <Radio innerCircleColor={"blue"} currentValue={this.state.value} value={2} onPress={this.handleOnPress.bind(this)}>
                <Text style={pagestyles.radioButtonText}>Ik wil een nieuw team starten</Text>
              </Radio>
            </View>

            <View style={{}}>
              <CheckBox
                center
                title='Ik accepteer hierbij de algemene voorwaarden'
                iconLeft
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                checked={this.state.accepted}
                containerStyle={{backgroundColor:'transparent'}}
                onPress={() => {
                  this.setState({
                    submitbtn:false,
                    accepted:true
                  });
                }}
              />
            </View>

            <Button
              disabled={this.state.submitbtn}
              onPress={({module ='inform'})  => this.handleSubmit(this)}
              buttonStyle={pagestyles.btnSubmit}
              title="REGISTREREN"
            />
        </View>

        <Modal
            isVisible={this.state.visibleModal === 1}
            animationIn={'slideInUp'}
            animationOut={'slideOutRight'}>
            <View style={pagestyles.modalContent}>
                <View style={{height:50}}>
                  <Text>Please Select Team</Text>
                  <View style={styles.hr}></View>
                </View>
                <Picker
                  style={pagestyles.picker}
                  selectedValue={this.state.selectedTeam}
                  onValueChange={ (itemValue, itemIndex) => this.setState({selectedTeam: itemValue})}>
                  {options.map((item, index) => {
                      return (<Picker.Item label={item} value={item} key={index}/>)
                  })}
                </Picker>
                <Button
                  buttonStyle={styles.btnGreen}
                  onPress={() => this.setState({visibleModal: 0})}
                  title="Ok"
                />
             </View>
        </Modal>
      </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
      marginTop:30,
      marginLeft:0,
      marginRight:0,
      left: 0,
      right: 0,
      width:'100%',
      backgroundColor: '#f2f2f2'
  },
  box: {
      paddingLeft:20,
      paddingRight:20
  },
  verticalSpacing: {
      height:20,
      backgroundColor: 'transparent'
  },
  buttonContainer:{
      height: 50,
      justifyContent: 'center',
  },
  buttonText: {
      fontSize: 18,
      fontFamily: 'Gill Sans',
      textAlign: 'center',
      margin: 10,
      color: '#ffffff',
      backgroundColor: 'transparent',
  },
  btnSubmit: {
    backgroundColor:'#64a932',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },
  radioButtonText: {
    paddingLeft:5
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  radioButtonContainer : {
      //flex:2,
      //flexDirection: 'row',
      //justifyContent: 'flex-start',
      //alignItems: 'center',
  },

});
