import React, { PureComponent } from 'react';
import { Platform, Image, View, StyleSheet } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';
//import { TabNavigator } from 'react-navigation';
//import Tabs from 'react-native-tabs';
//import RegistrationStepOne from "./tabone";
//import tabtwo from "./tabtwo";

const FirstRoute = () =>
  <View
    style={[ pagestyles.container, { backgroundColor: '#ff4081' } ]}
  />;
const SecondRoute = () =>
  <View
    style={[ pagestyles.container, { backgroundColor: '#673ab7' } ]}
  />;

export default class Registration extends PureComponent {

  state = {
    index: 0,
    routes: [
      { key: '1', title: 'First' },
      { key: '2', title: 'Second' },
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => <TabBar {...props} />;

  _renderScene = SceneMap({
    '1': FirstRoute,
    '2': SecondRoute,
  });

  render() {
      return (
        <View style={pagestyles.container}>
            <View style={pagestyles.upperboxContainer}>
                <View style={pagestyles.upperbox}>
                    <Image source={require('../../assets/logo.png')}
                      style={pagestyles.logo} />
                    <View style={pagestyles.horizontalSpacing}></View>
                </View>
            </View>

            <TabViewAnimated
              style={pagestyles.container}
              navigationState={this.state}
              renderScene={this._renderScene}
              renderHeader={this._renderHeader}
              onIndexChange={this._handleIndexChange}
            />
        </View>
      );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  tabbarView: {
      position:'absolute',
      bottom:0,
      right:0,
      left:0,
      height:50,
      opacity:1,
      backgroundColor:'transparent',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
  },
  upperboxContainer : {
      height:200,
  },
  backgroundColorWhite:{
      backgroundColor: 'white',
  },
  registerStepText : {
      textAlign: 'center'
  },
  registerStepContainer : {
      backgroundColor: 'white',
  },
  registerStep : {
      flex: 1,
      flexDirection: 'row',
      backgroundColor: 'white',
      borderRadius: 5,
      borderWidth: 1,
      borderColor: '#bdcadc',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginLeft:10,
      marginRight:10,
  },
  logo: {
      height:75,
      width:75,
      resizeMode: 'contain'
  },
  verticalSpacingWithColor: {
      width:20,
      backgroundColor: '#bdcadc'
  },
  verticalSpacing: {
      width:(Platform.OS === 'ios') ? 20 : 10,
      backgroundColor: 'transparent'
  },
  horizontalSpacing: {
      height:20,
      backgroundColor: 'transparent'
  },
  upperBoxContainer : {
      flex: 1,
      height:250,
  },
  upperbox : {
      flex: 1,
      height:150,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#FFFFFF'
  },
  triangleContainer : {
      height: 30,
      flexDirection: 'row',
      backgroundColor: 'transparent'
  },
  triangle: {
      flex:1,
      width: 0,
      height: 0,
      backgroundColor: 'transparent',
      borderStyle: 'solid',
      borderLeftWidth: 50,
      borderRightWidth: 50,
      borderBottomWidth: 30,
      borderLeftColor: 'transparent',
      borderRightColor: 'transparent',
      borderBottomColor: 'white',
      backgroundColor: 'transparent',
      transform: [
          {rotate: '180deg'}
      ]
  }
});
