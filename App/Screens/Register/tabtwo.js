import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';

import {
  Text,  Button, CheckBox
} from 'react-native-elements';

//import LinearGradient from 'react-native-linear-gradient';

import styles from '../../Components/Styles';

export default class RegistrationStepTwo extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Melder'
  }

  constructor(props){
      super(props);
      this.state = {
        accepted: false
      };
  }

  render() {
    return (
      <View style={pagestyles.container}>
        <View style={{
          flex:1,
          marginTop:0,
          flexDirection:'column',
          justifyContent: 'flex-start',
          alignItems: 'center',
        }}>
          <Image stuyle={{
            resizeMode: 'cover'
          }} source={require("../../assets/tabmarker.png")}/>
        </View>
        <View style={pagestyles.box}>
          <Text style={{
            fontSize:24,
            color: '#6e85a1'
          }}>Gebruikt u straat.info als vrijwilliger?</Text>
          <View style={pagestyles.verticalSpacing}/>
          <View style={pagestyles.verticalSpacing}/>
          <View style={pagestyles.navline}>
            <Button
              onPress={() => this.props.navigation.navigate('FirstCode', {name: 'Back'})}
              buttonStyle={styles.btnRed}
              title="Nee"
            />
            <Button
              onPress={() => this.props.navigation.navigate('Inform', {name: 'Request Code',parent: 'FirstCode'})}
              buttonStyle={styles.btnGreen}
              title="Ja"
            />
          </View>
          <View style={pagestyles.verticalSpacing}/>
          <View style={pagestyles.verticalSpacing}/>
          <View style={pagestyles.verticalSpacing}/>
          <Text style={{
            fontSize:18,
            color: '#6e85a1'
          }}>Als u vrijwilliger bent, bijvoorbeeld aan een BIT of BPT meedoet, kies dan “ja”. Als u bijvoorbeeld bij de overheid werkt en u de app voor uw werk wilt gebruiken, klik dan “nee”.</Text>
          <View style={pagestyles.verticalSpacing}/>
          <CheckBox
            center
            title='Ik accepteer hierbij de algemene voorwaarden'
            iconLeft
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checked={this.state.accepted}
            onPress={() => {
              this.props.navigation.navigate("TabThree")
            }}
          />
          <View style={pagestyles.verticalSpacing}/>
          <View style={pagestyles.verticalSpacing}/>
          <View style={pagestyles.verticalSpacing}/>
          <View style={pagestyles.verticalSpacing}/>
          <View style={pagestyles.verticalSpacing}/>
          </View>
      </View>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
      flex:1,
      marginTop:30,
      marginLeft:0,
      marginRight:0,
      flexDirection: 'column',
      left: 0,
      right: 0,
      width:'100%',
      backgroundColor: '#f2f2f2'
  },
  box: {
      paddingLeft:20,
      paddingRight:20
  },
  verticalSpacing: {
      height:20,
      backgroundColor: 'transparent'
  },
  buttonContainer:{
      height: 50,
      justifyContent: 'center',
  },
  buttonText: {
      fontSize: 18,
      fontFamily: 'Gill Sans',
      textAlign: 'center',
      margin: 10,
      color: '#ffffff',
      backgroundColor: 'transparent',
  },
  navline: {
    flex:2,
    flexDirection: 'row',
  },

});
