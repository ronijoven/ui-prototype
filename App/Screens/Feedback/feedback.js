import React, { Component } from 'react';
import {
  Platform,
  View,
  Image,
  Modal,
  TextInput,
  StyleSheet,
  ScrollView
}  from 'react-native';
import {
  Text,  Button, CheckBox
} from 'react-native-elements';

import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Banner from '../../Components/InnerBanner';
import styles from '../../Components/Styles';

const onButtonPress = (pass) => {
  pass.navigation.navigate('DrawerOpen')
};
const onBackPress = (pass,parent) => {
  pass.navigation.navigate(parent)
};

export default class FeedbackScreen extends React.Component {
  static navigationOptions = {
    tabBarLabel: 'Home',
    drawerIcon: ({tintColor}) => {
      return (
        <Image source={require('../../assets/i_feedback.png')}
        style={styles.sidemenuLogo} />
      );
    }
  }

  constructor(props){
    super(props)
    this.state = {
      accepted: false,
      submitbtn: true,
    };
  }

  render() {
    const { region } = this.props;
    console.log(region);

    return (
      <ScrollView>
      <View style={{
          marginTop: Platform.OS === 'ios' ? 30 : 0,
          width: '100%'
        }}>
        <Banner title="Feedback"
          onPress={ () => this.props.navigation.navigate('DrawerOpen')}
          onBack={ () => onBackPress(this.props,'Home') }
        />
        <View style ={pagestyles.container}>
          <TextInput
              style={pagestyles.inputbox}
              placeholder="Naam"
              multiline={false}
          />
          <TextInput
              style={pagestyles.inputbox}
              placeholder="E-mailadres"
              multiline={false}
          />
          <TextInput
              style={{
                  height:80,
                  borderColor: 'grey',
                  borderWidth: 1,
                  borderRadius: 10,
                  width:'75%',
                  marginTop: 10,
                  paddingLeft:10,
                  paddingRight:5,
              }}
              placeholder="Schrijf hier uw probleem of tip voor verbetering"
              multiline={true}
          />
          <CheckBox
            center
            title='Ik accepteer hierbij de algemene voorwaarden'
            iconLeft
            checkedIcon='dot-circle-o'
            uncheckedIcon='circle-o'
            checked={this.state.accepted}
            onPress={() => {
              this.setState({
                submitbtn:false,
                accepted:true
              });
            }}
          />

            <View style={styles.spacing}></View>
            <Button
              disabled={this.state.submitbtn}
              disabledStyle={{
                backgroundColor:'#9c9c9c'
              }}
              onPress={() => {
                this.props.navigation.navigate('Dashboard')
              }}
              buttonStyle={pagestyles.btnSubmit}
              title="Versturen"
            />

        </View>
      </View>
      </ScrollView>
    );
  }
}

const pagestyles = StyleSheet.create({
  container: {
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  inputbox : {
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 10,
    width:'75%',
    marginTop:10,
    paddingLeft:10,
    paddingRight:5,
    height:45,
    shadowColor: "black",
    shadowOpacity: 0.8,
    shadowRadius: 2,
    shadowOffset: {
        height: 1,
        width: 0,
    },
  },
  btnSubmit: {
    width: '100%',
    backgroundColor:'#64a932',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height:40
  },
});
