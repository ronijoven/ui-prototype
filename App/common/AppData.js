import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';


const AppData = {

    setUserInfo : function(user){
        AsyncStorage.setItem('userInfo', JSON.stringify(user));
    },

    setUsername : function(username){
        AsyncStorage.setItem('username', JSON.stringify(username));
    },

    setPassword : function(password){
        AsyncStorage.setItem('password', JSON.stringify(password));
    },

    async getUserInfo(){
        var value = await AsyncStorage.getItem('userInfo');
        return value;
    },

    async getUsername(){
        var value = await AsyncStorage.getItem('username');
        return value;
    },

    async getPassword(){
        var value = await AsyncStorage.getItem('password');
        return value;
    }
};

export default AppData;