
const Api = {
    login : function(usernameVal,passwordVal){
        return new Promise(function(resolve,reject){
            fetch('https://straatinfo-backend.herokuapp.com/v1/api/login', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username : usernameVal,
                    password : passwordVal
                })
            })
            .then((response) => response.json())
            .then((responseData) => {
                console.log("inside responsejson");
                console.log('response object:',responseData);
                resolve(responseData);
            })
            .catch((error) => {
                reject(error);
            });
        });
    },

    getTeam : function(team_id,team_leader_id,team_name,is_active,user_id){
        return new Promise(function(resolve,reject){
            fetch('https://straatinfo-backend.herokuapp.com/v1/api/team/?team_id='+ team_id +'team_leader_id=' + team_leader_id + '&team_name='+ team_name +'&is_active='+ is_active +'&user_id=' + user_id, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then((response) => response.json())
            .then((responseData) => {
                console.log("inside responsejson");
                console.log('response object:',responseData);
                resolve(responseData);
            })
            .catch((error) => {
                reject(error);
            });
        });
    },

    getReports : function(user_id,host_id,is_active,report_id,status,report_type,offset){
        return new Promise(function(resolve,reject){
            fetch('https://straatinfo-backend.herokuapp.com/v1/api/report/?user_id='+ user_id +'host_id=' + host_id + '&is_active='+ is_active +'&report_id='+ report_id +'&status=' + status + '&report_type='+ report_type + '&offset='+ offset, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then((response) => response.json())
            .then((responseData) => {
                console.log("inside responsejson");
                console.log('response object:',responseData);
                resolve(responseData);
            })
            .catch((error) => {
                reject(error);
            });
        });
    },

    getCoordinateAddress : function(lat,long){
        return new Promise(function(resolve,reject){
            fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+long+'&key=AIzaSyBhdMuDGBHDOuc8XLoP_72Rpw9JvfYkh1E', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            })
            .then((response) => response.json())
            .then((responseData) => {
                console.log("inside responsejson");
                console.log('response object:',responseData);
                resolve(responseData);
            })
            .catch((error) => {
                reject(error);
            });
        });
    },

    getCategories:function(host_id,access_token){
        return new Promise(function(resolve,reject){
            alert('https://straatinfo-backend.herokuapp.com/v1/api/category?host_id='+host_id);
            fetch('https://straatinfo-backend.herokuapp.com/v1/api/category?host_id='+host_id, {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization' : 'Bearer ' + access_token
                }
            })
            .then((response) => response.json())
            .then((responseData) => {
                console.log("inside responsejson");
                console.log('response object:',responseData);
                resolve(responseData);
            })
            .catch((error) => {
                reject(error);
            });
        });
    },

    register : function(){
        
    }

};

export default Api;